package jp.co.unirita;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrugalitasGateApplication {

	public static void main(String[] args) {
		SpringApplication.run(FrugalitasGateApplication.class, args);
	}
}
